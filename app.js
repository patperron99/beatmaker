class DrumKit {
	constructor() {
		this.pads = document.querySelectorAll('.pad');
		this.kickAudio = document.querySelector('.kick-sound');
		this.snareAudio = document.querySelector('.snare-sound');
		this.hihatAudio = document.querySelector('.hihat-sound');
		this.playBtn = document.querySelector('.play');
		this.index = 0;
		this.bpm = 150;
		this.isPlaying = null;
		this.soundSelects = document.querySelectorAll('select');
		this.kickSelected = document.querySelector('#kick-select')[0].value;
		this.snareSelected = document.querySelector('#snare-select')[0].value;
		this.hihatSelected = document.querySelector('#hihat-select')[0].value;
		this.muteBtn = document.querySelectorAll('.mute');
		this.sliderBar = document.querySelector('.tempo-slider');
		this.tempoText = document.querySelector('.tempo-nr');
	}
	activePad() {
		this.classList.toggle('active');
	}
	repeate() {
		let step = this.index % 8;
		const activeBars = document.querySelectorAll(`.b${step}`);
		activeBars.forEach((bar) => {
			bar.style.animation = `playTrack 0.3s alternate ease-in-out 2`;
			if (bar.classList.contains('active')) {
				if (bar.classList.contains('kick-pad')) {
					this.kickAudio.src = this.kickSelected;
					this.kickAudio.currentTime = 0;
					this.kickAudio.play();
				}
				if (bar.classList.contains('snare-pad')) {
					this.snareAudio.src = this.snareSelected;
					this.snareAudio.currentTime = 0;
					this.snareAudio.play();
				}
				if (bar.classList.contains('hihat-pad')) {
					this.hihatAudio.src = this.hihatSelected;
					this.hihatAudio.currentTime = 0;
					this.hihatAudio.play();
				}
			}
		});
		this.index++;
	}
	start() {
		const interval = (60 / this.bpm) * 1000;
		if (!this.isPlaying) {
			this.isPlaying = setInterval(() => {
				this.repeate();
			}, interval);
		} else {
			clearInterval(this.isPlaying);
			this.isPlaying = null;
		}
	}
	updateBtn() {
		if (!this.isPlaying) {
			this.playBtn.innerText = 'Play';
			this.playBtn.classList.remove('active');
		} else {
			this.playBtn.innerText = 'Stop';
			this.playBtn.classList.add('active');
		}
	}
	changeSound(e) {
		console.log(e);
		switch (e.target.name) {
			case 'kick-select':
				this.kickSelected = e.target.value;
				break;
			case 'snare-select':
				this.snareSelected = e.target.value;
				break;
			case 'hihat-select':
				this.hihatSelected = e.target.value;
				break;
		}
	}
	mute(e) {
		const muteIndex = e.target.getAttribute('data-track');

		e.target.classList.toggle('active');

		if (e.target.classList.contains('active')) {
			switch (muteIndex) {
				case '0':
					this.kickAudio.volume = 0;
					e.target.children[0].classList.add('fa-volume-xmark');
					break;
				case '1':
					this.snareAudio.volume = 0;
					e.target.children[0].classList.add('fa-volume-xmark');
					break;
				case '2':
					this.hihatAudio.volume = 0;
					e.target.children[0].classList.add('fa-volume-xmark');
					break;
			}
		} else {
			switch (muteIndex) {
				case '0':
					this.kickAudio.volume = 1;
					e.target.children[0].classList.remove('fa-volume-xmark');
					break;
				case '1':
					this.snareAudio.volume = 1;
					e.target.children[0].classList.remove('fa-volume-xmark');
					break;
				case '2':
					this.hihatAudio.volume = 1;
					e.target.children[0].classList.remove('fa-volume-xmark');
					break;
			}
		}
	}
	changeTempo(e) {
		drum.bpm = e.target.value;
		drum.tempoText.innerText = e.target.value;
	}
	updateTempo(e) {
		const playBtn = document.querySelector('.play');
		if (playBtn.classList.contains('active')) {
			clearInterval(this.isPlaying);
			this.isPlaying = null;
			this.start();
		}
	}
}

const drum = new DrumKit();

drum.playBtn.addEventListener('click', () => {
	drum.start();
	drum.updateBtn();
});

drum.pads.forEach((pad) => {
	pad.addEventListener('click', drum.activePad);
	pad.addEventListener('animationend', function () {
		this.style.animation = '';
	});
});

drum.soundSelects.forEach((soundSelect) => {
	soundSelect.addEventListener('change', function (e) {
		drum.changeSound(e);
	});
});

drum.muteBtn.forEach((btn) => {
	btn.addEventListener('click', function (e) {
		drum.mute(e);
	});
});

drum.sliderBar.addEventListener('input', (e) => {
	drum.changeTempo(e);
});

drum.sliderBar.addEventListener('change', (e) => {
	drum.updateTempo(e);
});
